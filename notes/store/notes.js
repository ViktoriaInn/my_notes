export const state = () => ({
  notes: []
})

export const mutations = {
  setNotes (state, notes) {
    state.notes = notes
  }
}

const url = 'http://afs.alt-point.ru:8080/notes'

async function doRequest ({ dispatch }, url, method, body) {
  await fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`Some error with ${method}`)
      }
    }).catch((error) => {
      console.error(error)
    })
  dispatch('nuxtServerInit')
}

export const actions = {
  async nuxtServerInit ({ commit }) {
    const notes = await fetch(url)
      .then(res => res.json())
    commit('setNotes', notes)
  },
  addNote ({ dispatch }, note) {
    doRequest({ dispatch }, url, 'POST', JSON.stringify(note))
  },
  editNote ({ dispatch }, note) {
    doRequest({ dispatch }, `${url}/${note.id}`, 'PUT', JSON.stringify(note))
  },
  deleteNote ({ dispatch }, id) {
    doRequest({ dispatch }, `${url}/${id}`, 'DELETE')
  }
}

export const getters = {
  notes: s => s.notes,
  noteByID: (state, getters) => id => getters.notes.find(e => e.id === id)
}
